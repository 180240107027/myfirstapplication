package com.aswdc.myfirstapplication.activity;

        import android.content.Intent;
        import android.graphics.Typeface;
        import android.net.Uri;
        import android.os.Bundle;
        import android.os.Handler;
        import android.os.Looper;
        import android.text.TextUtils;
        import android.util.Log;
        import android.view.Menu;
        import android.view.MenuInflater;
        import android.view.MenuItem;
        import android.view.View;
        import android.widget.Button;
        import android.widget.CheckBox;
        import android.widget.EditText;
        import android.widget.ImageView;
        import android.widget.RadioButton;
        import android.widget.RadioGroup;
        import android.widget.TextView;
        import android.widget.Toast;

        import androidx.annotation.Nullable;
        import androidx.appcompat.app.AppCompatActivity;


        import com.aswdc.myfirstapplication.MainActivity;
        import com.aswdc.myfirstapplication.R;
        import com.aswdc.myfirstapplication.activity.LoginDetailsActivity;
        import com.aswdc.myfirstapplication.database.MyDatabase;
        import com.aswdc.myfirstapplication.database.Tbl_UserData;
        import com.aswdc.myfirstapplication.util.Constant;

        import java.util.ArrayList;
        import java.util.HashMap;

public class LoginActivity extends AppCompatActivity {

    EditText etFirstName, etLastName, etPhoneNumber, etEmail;
    Button btnSubmit;
    ImageView ivClose, ivBackground;
    TextView tvDisplay;
    RadioGroup rgGender;
    RadioButton rbMale, rbFemale;
    CheckBox chbCricket, chbFootball, chbHokey;

    ArrayList<HashMap<String, Object>> userList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initViewReference();
        setTypeFaceOnView();
        intViewEvent();


    }

    void initViewReference() {

        new MyDatabase(LoginActivity.this).getReadableDatabase();

        etFirstName = findViewById(R.id.etActFirstName);
        etLastName = findViewById(R.id.etActLastName);
        etPhoneNumber = findViewById(R.id.etActPhoneNumber);
        etEmail = findViewById(R.id.etActEmail);

        btnSubmit = findViewById(R.id.btnActSubmit);

        ivClose = findViewById(R.id.ivActClose);
        ivBackground = findViewById(R.id.ivActBackground);

        tvDisplay = findViewById(R.id.tvActDisplay);

        rgGender = findViewById(R.id.rgActGender);

        rbMale = findViewById(R.id.rbActMale);
        rbFemale = findViewById(R.id.rbActFemale);

        chbCricket = findViewById(R.id.chbActCricket);
        chbFootball = findViewById(R.id.chbActFootball);
        chbHokey = findViewById(R.id.chbActHokey);

        getSupportActionBar().setTitle(R.string.lbl_login_activity);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.dashboard_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.opMenuAboutUs:
            case R.id.opMenuDeveloper:
                return true;
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    void setTypeFaceOnView() {

        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/island_moments.ttf" );

        etFirstName.setTypeface(typeface, Typeface.BOLD);
        etLastName.setTypeface(typeface, Typeface.BOLD);
        etEmail.setTypeface(typeface, Typeface.BOLD);
        etPhoneNumber.setTypeface(typeface, Typeface.BOLD);
        btnSubmit.setTypeface(typeface, Typeface.BOLD);
        tvDisplay.setTypeface(typeface, Typeface.BOLD);
        rbMale.setTypeface(typeface, Typeface.BOLD);
        rbFemale.setTypeface(typeface, Typeface.BOLD);
        chbCricket.setTypeface(typeface, Typeface.BOLD);
        chbFootball.setTypeface(typeface, Typeface.BOLD);
        chbHokey.setTypeface(typeface, Typeface.BOLD);


    }

    void intViewEvent() {

        rgGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (checkedId == R.id.rbActMale) {

                    chbCricket.setVisibility(View.VISIBLE);
                    chbFootball.setVisibility(View.VISIBLE);
                    chbHokey.setVisibility(View.VISIBLE);

                }

                else if (checkedId == R.id.rbActFemale) {

                    chbHokey.setVisibility(View.GONE);


                }
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (setValidationOnView()){

                    String name = etFirstName.getText().toString() + " " + etLastName.getText().toString();
                    String phoneNumber = etPhoneNumber.getText().toString();
                    String emailAddress = etEmail.getText().toString();
                    String gender = rbMale.isChecked() ? rbMale.getText().toString() : rbFemale.getText().toString();
                    String hobby = "";
                    if (chbCricket.isChecked()) {
                        hobby += "," + chbCricket.getText().toString();
                    }
                    if (chbFootball.isChecked()) {
                        hobby += "," + chbFootball.getText().toString();
                    }
                    if (chbHokey.isChecked()) {
                        hobby += "," + chbHokey.getText().toString();
                    }
                    hobby = hobby.substring(1);

                    Tbl_UserData tblUserData = new Tbl_UserData(LoginActivity.this);
                    long lastInserted = tblUserData.insertUserDetails(name, phoneNumber, emailAddress, gender, hobby);
                    Toast.makeText(getApplicationContext(), lastInserted > 0 ? "User Inserted Successfully" : "Something Went Wrong", Toast.LENGTH_SHORT).show();




                    /*HashMap<String, Object> map = new HashMap<>();
                    map.put(Constant.FIRST_NAME, etFirstName.getText().toString());
                    map.put(Constant.LAST_NAME, etLastName.getText().toString());
                    map.put(Constant.PHONE_NUMBER, etPhoneNumber.getText().toString());
                    map.put(Constant.EMAIL_ADDRESS, etEmail.getText().toString());
                    map.put(Constant.GENDER, rbMale.isChecked() ? rbMale.getText().toString() : rbFemale.getText().toString());

                    String hobbies = "";
                    if (chbCricket.isChecked()) {
                        hobbies += "," + chbCricket.getText().toString();
                    }
                    if (chbFootball.isChecked()) {
                        hobbies += "," + chbFootball.getText().toString();
                    }
                    if (chbHokey.isChecked()) {
                        hobbies += "," + chbHokey.getText().toString();
                    }
                    map.put(Constant.HOBBY, hobbies);

                    userList.add(map);

                    Intent intent = new Intent(LoginActivity.this, LoginDetailsActivity.class);
                    intent.putExtra("UserList", userList);
                    startActivity(intent);

                    resetViewOnClick();*/

               /* Integer intFirstName = 0;
                try {
                    intFirstName = Integer.valueOf(etFirstName.getText().toString());
                } catch (NumberFormatException nfe) {
                    System.out.println("Could not parse " + nfe);
                }
                String lastName = etLastName.getText().toString();
                String concatTempString = intFirstName+ " "+ lastName;

                Toast.makeText(getApplicationContext(),rbMale.isChecked()?"Male":"Female",Toast.LENGTH_SHORT).show();


                Intent intent = new Intent(LoginActivity.this, LoginDetailsActivity.class);
                intent.putExtra("loginData", concatTempString);
                startActivity(intent);
                finish();*/

                }


            }
        });

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();

               /* Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.com"));
                startActivity(intent);*/

            }
        });



        /*ArrayList<HashMap<String, Object>> al = new ArrayList<>();

        HashMap<String, Object> map = new HashMap<>();
        map.put("userName", 1);
        map.put("FirstName", "Jayrajsinh");
        map.put("LastName", "Chavada");
        al.add(map);

        HashMap<String, Object> map1 = new HashMap<>();
        map1.put("userName", 2);
        map1.put("FirstName", "Ravirajsinh");
        map1.put("LastName", "Chavada");
        al.add(map1);

        for (int i = 0; i < al.size(); i++){

            Log.d("UserList::", ""+al.get(i).get("FirstName") + "::" + al.get(i).get("LastName"));

        }*/


    }

    void resetViewOnClick() {

        etFirstName.setText("");
        etFirstName.requestFocus();
        etLastName.setText("");
        etPhoneNumber.setText("");
        etEmail.setText("");
        rbMale.setChecked(false);
        rbFemale.setChecked(true);
        chbCricket.setChecked(false);
        chbFootball.setChecked(false);
        chbHokey.setChecked(false);


    }

    boolean setValidationOnView() {

        boolean flag = true;

        // FirstName Validation
        if(TextUtils.isEmpty(etFirstName.getText().toString().trim())) {

            etFirstName.setError(getString(R.string.error_lbl_enter_value));
            etFirstName.requestFocus();
            flag = false;


        } else {

            String firstNamePattern = "[a-zA-Z]+\\\\?";
            if (!etFirstName.getText().toString().trim().matches(firstNamePattern)) {

                etFirstName.setError(getString(R.string.error_lbl_enter_valid_name));
                etFirstName.requestFocus();
                flag = false;

            }

        }

        // LastName Validation
        if(TextUtils.isEmpty(etLastName.getText().toString().trim())) {

            etLastName.setError(getString(R.string.error_lbl_enter_value));
            etLastName.requestFocus();
            flag = false;

        } else {

            String firstNamePattern = "[a-zA-Z]+\\\\?";
            if (!etLastName.getText().toString().trim().matches(firstNamePattern)) {

                etLastName.setError(getString(R.string.error_lbl_enter_valid_name));
                etLastName.requestFocus();
                flag = false;

            }

        }

        // Phone Number Validation
        if(TextUtils.isEmpty(etPhoneNumber.getText().toString().trim())) {

            etPhoneNumber.setError(getString(R.string.error_lbl_enter_value));
            etPhoneNumber.requestFocus();
            flag = false;

        } else {

            String phoneNumberPattern1 = "[0-9]+\\\\?";
            String phoneNumberPattern2 = "[+][0-9]+\\\\?";

            if (etPhoneNumber.getText().toString().trim().length()<10 ||
                    ( !etPhoneNumber.getText().toString().trim().matches(phoneNumberPattern1)
                            && !etPhoneNumber.getText().toString().trim().matches(phoneNumberPattern2) )) {

                etPhoneNumber.setError(getString(R.string.error_lbl_enter_valid_number));
                etPhoneNumber.requestFocus();
                flag = false;

            }

        }

        // Email Address Validation
        if(TextUtils.isEmpty(etEmail.getText().toString().trim())) {

            etEmail.setError(getString(R.string.error_lbl_enter_value));
            etEmail.requestFocus();
            flag = false;

        } else {

            String emailAddressPattern1 = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+\\.+[a-z]+";
            String emailAddressPattern2 = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

            if (!etEmail.getText().toString().trim().matches(emailAddressPattern1)
                    && !etEmail.getText().toString().trim().matches(emailAddressPattern2)) {

                etEmail.setError(getString(R.string.error_lbl_enter_valid_email_address));
                etEmail.requestFocus();
                flag = false;

            }

        }

        // Check Box Validation
        if(!chbCricket.isChecked() && !chbFootball.isChecked() && !chbHokey.isChecked()) {

            Toast.makeText(LoginActivity.this, "Please Select Check Box !",Toast.LENGTH_LONG).show();
            flag = false;
        }


        return flag;
    }

    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed(){
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

}

