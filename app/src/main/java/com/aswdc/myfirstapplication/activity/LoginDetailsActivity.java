package com.aswdc.myfirstapplication.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.aswdc.myfirstapplication.R;
import com.aswdc.myfirstapplication.adapter.UserListAdapter;
import com.aswdc.myfirstapplication.util.Constant;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;


public class LoginDetailsActivity extends AppCompatActivity {

    ListView lvUserList;
    ArrayList<HashMap<String, Object>> userList = new ArrayList<>();
    UserListAdapter userListAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_details);

        initViewReference();
        bindViewValue();


    }

    void initViewReference() {

        lvUserList = findViewById(R.id.lvActUserList);


    }

    void bindViewValue() {

        userList.addAll((Collection<? extends HashMap<String, Object>>) getIntent().getSerializableExtra("UserList"));
        userListAdapter = new UserListAdapter(this, userList);
        lvUserList.setAdapter(userListAdapter);

        lvUserList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Toast.makeText(LoginDetailsActivity.this, userList.get(position).get(Constant.GENDER).toString(), Toast.LENGTH_LONG).show();

            }
        });

    }


}
