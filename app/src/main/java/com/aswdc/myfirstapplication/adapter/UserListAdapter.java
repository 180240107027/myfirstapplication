package com.aswdc.myfirstapplication.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.aswdc.myfirstapplication.R;
import com.aswdc.myfirstapplication.activity.LoginDetailsActivity;
import com.aswdc.myfirstapplication.util.Constant;

import java.util.ArrayList;
import java.util.HashMap;


public class UserListAdapter extends BaseAdapter {

    ViewHolder holder;
    Context context;
    ArrayList<HashMap<String, Object>> userList;

    public UserListAdapter(Context context, ArrayList<HashMap<String, Object>> userList) {

        this.context = context;
        this.userList = userList;

    }


    @Override
    public int getCount() {
        return userList.size();
    }

    @Override
    public Object getItem(int position) {
        return userList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        if (convertView == null) {

            holder = new ViewHolder();

            convertView = LayoutInflater.from(context).inflate(R.layout.view_row_user_list, parent, false);
            holder.tvName = convertView.findViewById(R.id.tvLstName);
            holder.tvPhoneNumber = convertView.findViewById(R.id.tvLstPhoneNumber);
            holder.tvEmail = convertView.findViewById(R.id.tvLstEmail);
            holder.tvGender = convertView.findViewById(R.id.tvLstGender);

            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }




        holder.tvName.setText( userList.get(position).get(Constant.FIRST_NAME) + " " +userList.get(position).get(Constant.LAST_NAME));
        holder.tvPhoneNumber.setText(String.valueOf(userList.get(position).get(Constant.PHONE_NUMBER)));
        holder.tvEmail.setText(String.valueOf(userList.get(position).get(Constant.EMAIL_ADDRESS)));
        holder.tvGender.setText(String.valueOf(userList.get(position).get(Constant.GENDER)));


        if(holder.tvGender.getText().toString().trim().matches("Male"))
        {
            holder.tvGender.setText("M");
            holder.tvGender.setBackgroundResource(R.drawable.ic_male_background);

        } else{

            holder.tvGender.setText("F");
            holder.tvGender.setBackgroundResource(R.drawable.ic_female_background);

        }

  /*      if (position % 2 == 1) {
            view1.setBackgroundColor(Color.);
        } else {
            view1.setBackgroundColor(Color.CYAN);
        }*/


        return convertView;
    }

    class ViewHolder {

        TextView tvName, tvPhoneNumber, tvEmail, tvGender;

    }

}
