package com.aswdc.myfirstapplication.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class Tbl_UserData extends MyDatabase{

    public static final String TBL_USER_DATA = "Tbl_UserData";
    public static final String USER_ID = "UserId";
    public static final String NAME = "Name";
    public static final String PHONE_NUMBER = "PhoneNumber";
    public static final String EMAIL_ADDRESS = "EmailAddress";
    public static final String GENDER = "Gender";
    public static final String HOBBY = "Hobby";

    public Tbl_UserData(Context context) {
        super(context);
    }

    public long insertUserDetails(String name, String phoneNumber, String emailAddress, String gender, String hobby) {

        long insertedId = 0;


        if (isNumberAvailable(phoneNumber)) {

            insertedId = -1;

        } else {

            SQLiteDatabase db = getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(NAME, name);
            cv.put(PHONE_NUMBER, phoneNumber);
            cv.put(EMAIL_ADDRESS, emailAddress);
            cv.put(GENDER, gender);
            cv.put(HOBBY, hobby);
            insertedId = db.insert(TBL_USER_DATA, null, cv);
            db.close();

        }

        return insertedId ;

    }


    public boolean isNumberAvailable(String phoneNumber) {

        SQLiteDatabase db = getReadableDatabase();
        String query = " SELECT * FROM "+ TBL_USER_DATA + " WHERE " + PHONE_NUMBER + " = ? ";
        Cursor cursor = db.rawQuery(query,new String[]{ phoneNumber });
        cursor.moveToFirst();
        boolean isNumberAvailable = cursor.getCount() > 0;
        cursor.close();
        db.close();
        return  isNumberAvailable;

    }
}
